'''
************************************************************************************************
* Filename    : main.py
* Progammers  : Xiam Schurink
* Description : Maak een simpele qrcode
* Gitlab      : https://gitlab.com/xiams
* Copyright   : (C) Copyright 2024, Xiam Schurink, Leeuwarden, Friesland.
*               All rights reserved
************************************************************************************************
'''


import qrcode

url = input("Van welke url wil je een QRCode? ")
naam_img = input("hoe wil je de QRcode noemen? ")
full_naam_img = naam_img + ".png"

img = qrcode.make(url)
img.save(full_naam_img)
